# Webservice statistics

This service provides access to the RESIF-DC statistics.


## Query usage

    /query? (request-options) [channel-options] [date-options] [storage-options] [send-options] [output-options] [nodata=404]

    where :

    request-options    ::  (request=<country|map|send|storage|timeseries>)
    channel-options    ::  [net=<network> sta=<station> loc=<location> cha=<channel>]
    date-options       ::  [starttime=<date>] [endtime=<date>]
    storage-options    ::  [year=<year>] [type=<buffer|validated>]
    send-options       ::  [ranklimit=<number>] [sum=<boolean>]
    other-options      ::  [country=<country_code>] [media=<dataselect|seedlink|station>]
    output-options     ::  [format=<csv|json|text>]

    (..) required
    [..] optional

## Sample queries

<a href="http://ws.resif.fr/resifws/statistics/1/query?request=storage&net=FR&year=2020">http://ws.resif.fr/resifws/statistics/1/query?request=storage&net=FR&year=2020</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=storage&net=Z32015&year=2016">http://ws.resif.fr/resifws/statistics/1/query?request=storage&net=Z32015&year=2016</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=storage&net=FR&year=2018&type=validated">http://ws.resif.fr/resifws/statistics/1/query?request=storage&net=FR&year=2018&type=validated</a>

<a href="http://ws.resif.fr/resifws/statistics/1/query?request=send&media=dataselect&net=FR">http://ws.resif.fr/resifws/statistics/1/query?request=send&media=dataselect&net=FR</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=send&media=dataselect&net=FR&country=IT">http://ws.resif.fr/resifws/statistics/1/query?request=send&media=dataselect&net=FR&country=IT</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=send&net=FR,RA&starttime=2020-01-01&ranklimit=5">http://ws.resif.fr/resifws/statistics/1/query?request=send&net=FR,RA&starttime=2020-01-01&ranklimit=5</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=send&net=FR,RA&starttime=2020-01-01&endtime=2020-12-31&sum=true">http://ws.resif.fr/resifws/statistics/1/query?request=send&net=FR,RA&starttime=2020-01-01&endtime=2020-12-31&sum=true</a>

<a href="http://ws.resif.fr/resifws/statistics/1/query?request=country&net=RA,ND">http://ws.resif.fr/resifws/statistics/1/query?request=country&net=RA,ND</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=country&net=RA,ND&country=all">http://ws.resif.fr/resifws/statistics/1/query?request=country&net=RA,ND&country=all</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=country&net=RA,ND&country=IT,US,FR">http://ws.resif.fr/resifws/statistics/1/query?request=country&net=RA,ND&country=IT,US,FR</a>

<a href="http://ws.resif.fr/resifws/statistics/1/query?request=map&starttime=2020-01-01">http://ws.resif.fr/resifws/statistics/1/query?request=map&starttime=2020-01-01</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=map&starttime=2020-01-01&net=CL">http://ws.resif.fr/resifws/statistics/1/query?request=map&starttime=2020-01-01&net=CL</a>

<a href="http://ws.resif.fr/resifws/statistics/1/query?request=timeseries&starttime=2020-01-01">http://ws.resif.fr/resifws/statistics/1/query?request=timeseries&starttime=2020-01-01</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=timeseries&starttime=2020-01-01&country=all">http://ws.resif.fr/resifws/statistics/1/query?request=timeseries&starttime=2020-01-01&country=all</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=timeseries&starttime=2020-01-01&country=all&media=dataselect">http://ws.resif.fr/resifws/statistics/1/query?request=timeseries&starttime=2020-01-01&country=all&media=dataselect</a>


### timeseries request output example

time       country bytes         clients\
2020-01-06 AT      116539392     1\
2020-01-06 AU      12664213504   1\
...

The 'time' field represents the first day of a period of one week.


## Detailed descriptions of each query parameter

| Parameter  | Example  | Discussion                                                                       |
| :--------- | :------- | :--------------------------------------------------------------------------------|
| net[work]  | FR       | Seismic network name. Accepts wildcards and lists.                               |
| sta[tion]  | CIEL     | Station name. Accepts wildcards and lists.                                       |
| loc[ation] | 00       | Location code. Use loc=-- for empty location codes. Accepts wildcards and lists. |
| cha[nnel]  | HHZ      | Channel Code. Accepts wildcards and lists.                                       |             |
| start[time] | 2010-01-10T00:00:00 | Specify the responses that cover the time period after (and including) this time. |
| end[time]  | 2011-02-11T01:00:00 | Specify responses that cover the time period prior to this time.    |
| format     | text     | The output format. Accepted values are text (the default), json, and csv.      |
| request    | send     | Type of statistic requested. Allowed values : <br> - __country__ : the number of requests from each country <br> - __map__ : a color coded map with the number of requests from each country<br> - __send__ : the volume of distributed data by protocol <br> - __storage__ : the volume of data stored <br> - __timeseries__ : returns a timeseries with the country, the number and the volume of requests |

### Additional parameters for storage request option
| Parameter  | Example  | Discussion                                                                      |
| :--------- | :------- | :------------------------------------------------------------------------------ |
| type       | validated | Choose validated data or not. Allowed values : __buffer__, and __validated__ (all by default) |
| year       | 2019,2020 | List of years for which the volume of data is requested.                       |

### Additional parameters for send request option
| Parameter  | Example | Discussion                                                                      |
| :--------- | :------ | :------------------------------------------------------------------------------ |
| ranklimit  | 5       | Give only the largest stations of each network.                                 |
| sum        | true    | Give the total amount of data on selected network.                              |

### Additional parameters for country, send, and timeseries request options
| Parameter  | Example  | Discussion                                                                      |
| :--------- | :------- | :------------------------------------------------------------------------------ |
| country    | FR,GB    | Data are filtered to their country of origin. Use the country code (ISO 3166-1 alpha-2). The value __all__ returns the sum on all country. |
| media      | seedlink | Protocol used to request data. Allowed values : __dataselect__, __seedlink__, and __station__ (all by default) |

## Date and time formats

    YYYY-MM-DDThh:mm:ss[.ssssss] ex. 1997-01-31T12:04:32.123
    YYYY-MM-DD ex. 1997-01-31 (a time of 00:00:00 is assumed)

    where:

    YYYY    :: four-digit year
    MM      :: two-digit month (01=January, etc.)
    DD      :: two-digit day (01 through 31)
    T       :: date-time separator
    hh      :: two-digit hour (00 through 23)
    mm      :: two-digit number of minutes (00 through 59)
    ss      :: two-digit number of seconds (00 through 59)
    ssssss  :: one to six-digit number of microseconds (0 through 999999)

## Source code

<a href="https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/ws-statistics">https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/ws-statistics</a>

## Report a problem

<a href="https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/ws-statistics/-/issues/">https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/ws-statistics/-/issues/</a>

