# Webservice statistics

Ce service donne accès aux statistiques de RESIF-DC.


## Utilisation de la requête

    /query? (request-options) [channel-options] [date-options] [storage-options] [send-options] [output-options] [nodata=404]

    où :

    request-options    ::  (request=<country|map|send|storage|timeseries>)
    channel-options    ::  [net=<network> sta=<station> loc=<location> cha=<channel>]
    date-options       ::  [starttime=<date>] [endtime=<date>]
    storage-options    ::  [year=<year>] [type=<buffer|validated>]
    send-options       ::  [ranklimit=<number>] [sum=<boolean>]
    other-options      ::  [country=<country_code>] [media=<dataselect|seedlink|station>]
    output-options     ::  [format=<csv|json|text>]

    (..) requis
    [..] optionnel

## Exemples de requêtes

<a href="http://ws.resif.fr/resifws/statistics/1/query?request=storage&net=FR&year=2020">http://ws.resif.fr/resifws/statistics/1/query?request=storage&net=FR&year=2020</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=storage&net=Z32015&year=2016">http://ws.resif.fr/resifws/statistics/1/query?request=storage&net=Z32015&year=2016</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=storage&net=FR&year=2018&type=validated">http://ws.resif.fr/resifws/statistics/1/query?request=storage&net=FR&year=2018&type=validated</a>

<a href="http://ws.resif.fr/resifws/statistics/1/query?request=send&media=dataselect&net=FR">http://ws.resif.fr/resifws/statistics/1/query?request=send&media=dataselect&net=FR</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=send&media=dataselect&net=FR&country=IT">http://ws.resif.fr/resifws/statistics/1/query?request=send&media=dataselect&net=FR&country=IT</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=send&net=FR,RA&starttime=2020-01-01&ranklimit=5">http://ws.resif.fr/resifws/statistics/1/query?request=send&net=FR,RA&starttime=2020-01-01&ranklimit=5</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=send&net=FR,RA&starttime=2020-01-01&endtime=2020-12-31&sum=true">http://ws.resif.fr/resifws/statistics/1/query?request=send&net=FR,RA&starttime=2020-01-01&endtime=2020-12-31&sum=true</a>

<a href="http://ws.resif.fr/resifws/statistics/1/query?request=country&net=RA,ND">http://ws.resif.fr/resifws/statistics/1/query?request=country&net=RA,ND</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=country&net=RA,ND&country=all">http://ws.resif.fr/resifws/statistics/1/query?request=country&net=RA,ND&country=all</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=country&net=RA,ND&country=IT,US,FR">http://ws.resif.fr/resifws/statistics/1/query?request=country&net=RA,ND&country=IT,US,FR</a>

<a href="http://ws.resif.fr/resifws/statistics/1/query?request=map&starttime=2020-01-01">http://ws.resif.fr/resifws/statistics/1/query?request=map&starttime=2020-01-01</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=map&starttime=2020-01-01&net=CL">http://ws.resif.fr/resifws/statistics/1/query?request=map&starttime=2020-01-01&net=CL</a>

<a href="http://ws.resif.fr/resifws/statistics/1/query?request=timeseries&starttime=2020-01-01">http://ws.resif.fr/resifws/statistics/1/query?request=timeseries&starttime=2020-01-01</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=timeseries&starttime=2020-01-01&country=all">http://ws.resif.fr/resifws/statistics/1/query?request=timeseries&starttime=2020-01-01&country=all</a>\
<a href="http://ws.resif.fr/resifws/statistics/1/query?request=timeseries&starttime=2020-01-01&country=all&media=dataselect">http://ws.resif.fr/resifws/statistics/1/query?request=timeseries&starttime=2020-01-01&country=all&media=dataselect</a>


### Exemple de sortie avec la requête timeseries

time       country bytes         clients\
2020-01-06 AT      116539392     1\
2020-01-06 AU      12664213504   1\
...

La colonne 'time' représente le premier jour d'une période d'une semaine.


## Descriptions détaillées de chaque paramètre de la requête

| Paramètre  | Exemple | Discussion                                                                    |
| :--------- | :------ | :---------------------------------------------------------------------------- |
| net[work]  | FR      | Nom du réseau sismique. Accepte les jokers et les listes.                     |
| sta[tion]  | CIEL    | Nom de la station. Accepte les jokers et les listes.                          |
| loc[ation] | 00      | Code de localisation. Utilisez loc=-- pour les codes de localisations vides. Accepte les jokers et les listes. |
| cha[nnel]  | HHZ     | Code de canal. Accepte les jokers et les listes.                              |
| start[time] | 2010-01-10T00:00:00 | Donne les réponses qui couvrent la période après la date donnée incluse. |
| end[time]  | 2011-02-11T01:00:00 | Donne les réponses qui couvrent la période avant la date donnée incluse. |
| format     | text    | Format de sortie. Valeurs autorisées : text, json et csv.                           |
| request    | send    | Type de la statistique demandée. Valeurs autorisées : <br> - __country__ : nombre de requêtes passées par pays <br> - __map__ : une carte avec le nombre de requêtes provenant de chaque pays <br> - __send__ : volume de données distribuées par protocoles <br> - __storage__ : quantité de données stockées brutes et validées <br> - __timeseries__ : produit une série temporelle avec en colonne le pays, le nombre et la taille cumulés des requêtes |

### Options supplémentaires spécifiques à la requête storage
| Paramètre  | Exemple | Discussion                                                                      |
| :--------- | :------ | :------------------------------------------------------------------------------ |
| type       | validated | Choix entre les données stockées validées ou non. Valeurs autorisées : __buffer__ et __validated__ (toutes par défaut) |
| year       | 2019,2020 | Liste des années pour lesquelles on demande la quantité de données. |

### Options supplémentaires spécifiques à la requête send
| Paramètre  | Exemple | Discussion                                                                      |
| :--------- | :------ | :------------------------------------------------------------------------------ |
| ranklimit  | 5       | Limite les réponses aux stations les plus volumineuses de chaque réseau.        |
| sum        | true    | Donne la somme des volumes de données sur les réseaux sélectionnés.             |

### Options supplémentaires spécifiques aux requêtes country, send et timeseries
| Paramètre  | Exemple | Discussion                                                                      |
| :--------- | :------ | :------------------------------------------------------------------------------ |
| country    | FR,GB   | Filtre les réponses avec une liste des pays d'origine. Utilisez l'abréviation en deux lettres du pays (ISO 3166-1 alpha-2). La valeur __all__ donne la somme sur tous les pays. |
| media      | seedlink | Protocoles utilisés pour la requête. Valeurs autorisées : __dataselect__, __seedlink__ et __station__ (toutes par défaut)|

## Formats des dates et des heures

    YYYY-MM-DDThh:mm:ss[.ssssss] ex. 1997-01-31T12:04:32.123
    YYYY-MM-DD ex. 1997-01-31 (une heure de 00:00:00 est supposée)

    avec :

    YYYY    :: quatre chiffres de l'année
    MM      :: deux chiffres du mois (01=Janvier, etc.)
    DD      :: deux chiffres du jour du mois (01 à 31)
    T       :: séparateur date-heure
    hh      :: deux chiffres de l'heure (00 à 23)
    mm      :: deux chiffres des minutes (00 à 59)
    ss      :: deux chiffres des secondes (00 à 59)
    ssssss  :: un à six chiffres des microsecondes en base décimale (0 à 999999)

## Code source

<a href="https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/ws-statistics">https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/ws-statistics</a>

## Reporter un problème

<a href="https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/ws-statistics/-/issues/">https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/ws-statistics/-/issues/</a>

