import json
import logging
import re
import time
from datetime import datetime

from bokeh.embed import file_html
from bokeh.models import ColorBar, LogColorMapper, LogTicker, NumeralTickFormatter
from bokeh.palettes import Plasma256, Viridis256
from bokeh.plotting import figure
from bokeh.resources import CDN

from flask import current_app, make_response
import psycopg2

from apps.globals import Error
from apps.globals import MEDIA
from apps.isoalpha2 import codes
from apps.utils import error_request
from apps.utils import tictac


def humanize_size(size_in_bytes):
    index = 0
    size_units = ["B", "KB", "MB", "GB", "TB", "PB", ""]
    while size_in_bytes >= 1024 and index < 5:
        size_in_bytes /= 1024
        index += 1
    return f"{round(size_in_bytes, 2)} {size_units[index]}"


def is_like_or_equal(params, key):
    """Builds the condition for the specified key in the "where" clause taking into account lists or wildcards."""

    subquery = list()
    for param in params[key].split(","):
        op = "LIKE" if re.search(r"[*?]", param) else "="
        subquery.append(f"{key} {op} '{param}'")
    return " OR ".join(subquery)


def sum_request(table_name):
    # ringserver n'a pas de requests_set
    # station et dataselect utilisent un request_set
    if table_name == 'ringserver_aggregate_weekly':
        req_column = "sum(requests)"
    else:
        req_column = "HLL_CARDINALITY(HLL_UNION_AGG(requests_set))::BIGINT"
    return req_column

def statistics_tables(media):
    """
    Renvoie la liste des tables de statistiques à interroger en fonction du paramètre media spécifié par l'utilisateur.
    Si l'utilisateur ne spécifie aucun média, alors on renvoie la liste.
    Utilise un mapping statique entre les medias et le nom des tables à utiliser.
    """
    tables = []
    statistics_tables = {
        'dataselect': 'dataselect_aggregate_weekly',
        'station': 'station_aggregate_weekly',
        'seedlink': 'ringserver_aggregate_weekly'
    }
    if media == None:
        tables = [v for _,v in statistics_tables.items()]
    else:
        tables.append(statistics_tables[media])
    return tables

def sql_common_string(params):
    # network, station, location, channel parameters
    s = f"""WHERE ({is_like_or_equal(params, "network")})"""
    if params["station"] != "*":
        s = f"""{s} AND ({is_like_or_equal(params, "station")})"""
    if params["location"] != "*" and params["request"] != "storage":
        s = f"""{s} AND ({is_like_or_equal(params, "location")})"""
    if params["channel"] != "*":
        s = f"""{s} AND ({is_like_or_equal(params, "channel")})"""

    if params["request"] != "storage":
        # country parameter
        if "all" not in params["country"] and params["country"] != "*":
            s = f"""{s} AND ({is_like_or_equal(params, "country")})"""

        # starttime, endtime parameters
        if params["start"]:
            start = datetime.strftime(params["start"], "%Y-%m-%d")
            s = f"""{s} AND bucket >= '{start}'"""
        if params["end"]:
            end = datetime.strftime(params["end"], "%Y-%m-%d")
            s = f"""{s} AND bucket <= '{end}'"""
    return s.replace("?", "_").replace("*", "%")


def country_sql_request(params):
    # La table à interroger dépend du média (dataselect, seedlink, station)
    # Si plusieurs medias, alors il faut faire plusieurs requêtes.
    s = "SELECT country, sum(requests) as requests, sum(clients) as clients FROM ("
    # Pour tous les pays ou pour un pays en particulier ?
    if params["country"] != "all":
        country_select = "COALESCE(country, 'n/a')"
    else:
        country_select = "'all'"

    for t in statistics_tables(params["media"]):
        s += f" SELECT {country_select} AS country, {sum_request(t)} as requests, HLL_CARDINALITY(HLL_UNION_AGG(clients))::INTEGER as clients FROM {t} {sql_common_string(params)} GROUP BY country UNION ALL"
    s = s[:-9]

    s += ') as sub GROUP BY country '
    if params["country"] != "all":
        s = f"{s} ORDER BY 2 DESC"
    return s


def map_sql_request(params):
    # La table à interroger dépend du média (dataselect, seedlink, station)
    # Si plusieurs medias, alors il faut faire plusieurs requêtes.

    select_list = []
    for t in statistics_tables(params["media"]):
        if t != 'station_aggregate_weekly':
            select_list.append(f"SELECT country, sum(bytes)::bigint as size FROM {t} {sql_common_string(params)} GROUP BY country")
    s = f"""
     SELECT country, size FROM (
         SELECT *, DENSE_RANK() OVER (PARTITION BY network ORDER BY size DESC) AS rank FROM (
             SELECT network, station, sum(size) as size FROM (
             { " UNION ALL ".join(select_list) }
             ) as sub group by 1,2
         ) as table1
     ) as table2  WHERE rank <= 2.0
    """
    logging.debug(f"Select request: {s}")
    return s


def send_sql_request(params):
    tables = statistics_tables(params["media"])
    # Attention, on s'intéresse à la quantité de données distribuée. Pour "station", on ne garde pas cette mesure, donc il faut l'exclure des tables
    if 'station_aggregate_weekly' in tables:
        tables.remove('station_aggregate_weekly')
    s = ""
    logging.debug(f"Tables to request: {tables}")
    for table in tables:
        if params["sum"]:
            s += f"""SELECT '{params["network"]}' AS network, 'all' AS station, SUM(bytes)::BIGINT AS size FROM {table} {sql_common_string(params)}"""
        else:
            s += f"SELECT network, station, SUM(bytes)::BIGINT AS size FROM {table} {sql_common_string(params)} GROUP BY network, station"
        s += " UNION ALL "
    s = s[:-10]
    if params["ranklimit"]:
       s = f"""
            SELECT network, station, size FROM (
                SELECT *, DENSE_RANK() OVER (PARTITION BY network ORDER BY size DESC) AS rank FROM (
                    SELECT network, station, sum(size) as size FROM (
                        {s}
                    ) as sub group by 1,2
                ) as table1
            ) as table2  WHERE rank <= {params["ranklimit"]}
       """
    else:
        s = f"SELECT network, station, sum(size) FROM ({s}) AS sub GROUP BY 1,2 ORDER BY 3 DESC"
    return s


def storage_sql_request(params):
    table = "dataholdings"
    columns = "year, network, station, channel, quality"
    s = f"SELECT DISTINCT ON ({columns}) {columns}, date, SUM(size)::BIGINT FROM {table}"
    s = f"{s} {sql_common_string(params)} AND channel IS NOT NULL"
    if params["type"]:
        s = f"""{s} AND ({is_like_or_equal(params, "type")})"""
        s = s.replace("buffer", "bud")
    if params["year"]:
        s = f"""{s} AND ({is_like_or_equal(params, "year")})"""
    # starttime, endtime parameters
    if params["start"]:
        start = datetime.strftime(params["start"], "%Y-%m-%d")
        s = f"""{s} AND date >= '{start}'"""
    if params["end"]:
        end = datetime.strftime(params["end"], "%Y-%m-%d")
        s = f"""{s} AND date <= '{end}'"""
    s = f"{s} GROUP BY {columns}, date ORDER BY {columns}, date DESC"
    return s


def timeseries_sql_request(params):
    table = "dataselect_aggregate_weekly"
    if params["country"] != "all":
        s = f"SELECT bucket, country, SUM(bytes)::BIGINT, HLL_CARDINALITY(HLL_UNION_AGG(clients))::INTEGER FROM {table}"
    else:
        s = f"SELECT bucket, 'all' AS country, SUM(bytes)::BIGINT, HLL_CARDINALITY(HLL_UNION_AGG(clients))::INTEGER FROM {table}"
    s = f"{s} {sql_common_string(params)}"
    if params["country"] != "all":
        s = f"{s} GROUP BY bucket, country ORDER BY bucket"
    else:
        s = f"{s} GROUP BY bucket ORDER BY bucket"
    return s


def sql_request(params):
    """Builds the PostgreSQL request"""

    if params["request"] == "country":
        s = country_sql_request(params)
    elif params["request"] == "map":
        s = country_sql_request(params)
    elif params["request"] == "send":
        s = send_sql_request(params)
    elif params["request"] == "storage":
        s = storage_sql_request(params)
    elif params["request"] == "timeseries":
        s = timeseries_sql_request(params)

    logging.info(f"Request composed: {s}")
    return s


def collect_data(params):
    """Get the result of the SQL query."""

    logging.debug("Start collecting data...")
    with psycopg2.connect(current_app.config["DATABASE_URI"]) as conn:
        logging.debug(conn.get_dsn_parameters())
        logging.debug(f"Postgres version : {conn.server_version}")
        with conn.cursor() as curs:
            select = sql_request(params)
            logging.debug(select)
            curs.execute(select)
            logging.debug(curs.statusmessage)
            return curs.fetchall()


def format_results(params, data):
    data = list(map(list, data))
    if params["format"] == "text":
        if params["request"] == "country":
            for row in data:
                row[1] = format(row[1], ",") if row[1] else None
                row[2] = format(row[2], ",") if row[2] else None
        elif params["request"] == "send":
            for row in data:
                if row[2]:
                    row[2] = format(row[2], ",") + f" ({humanize_size(row[2])})"
    data = [[str(val) for val in row] for row in data]
    return data


def get_header(params):
    if params["request"] == "storage":
        header = ["year", "network", "station", "channel", "quality", "lastupdated"]
        header.append("size")
    elif params["request"] == "country":
        header = ["country", "requests", "clients"]
    elif params["request"] == "send":
        header = ["network", "station", "bytes"]
    elif params["request"] == "timeseries":
        header = ["time", "country", "bytes", "clients"]
    return header


def get_column_widths(data, header=None):
    """Find the maximum width of each column"""
    ncols = range(len(data[0]))
    colwidths = [max([len(r[i]) for r in data]) for i in ncols]
    if header:
        colwidths = [max(len(h), cw) for h, cw in zip(header, colwidths)]
    return colwidths


def records_to_text(params, data, sep=" "):
    text = ""
    header = get_header(params)
    if params["format"] == "text":
        sizes = get_column_widths(data, header)
        # pad header and rows according to the maximum column width
        header = [val.ljust(sz) for val, sz in zip(header, sizes)]
        for row in data:
            row[:] = [val.ljust(sz) for val, sz in zip(row, sizes)]

    text = sep.join(header) + "\n"
    data = [f"{sep.join(row)}\n" for row in data]
    text += "".join(data)
    return text


def records_to_json(params, data):
    """Create json output"""

    header = [h.lower() for h in get_header(params)]
    dictlist = [dict(zip(header, row)) for row in data]
    data = {
        "created": datetime.utcnow().isoformat(timespec="seconds") + "Z",
        "datasources": dictlist,
    }
    return json.dumps(data, sort_keys=False)


def get_response(params, data):
    tic = time.time()
    fname = "resifws-statistics"
    if params["format"] == "text":
        response = make_response(records_to_text(params, data))
        response.headers["Content-type"] = "text/plain"
    elif params["format"] == "csv":
        headers = {"Content-Disposition": f"attachment; filename={fname}.csv"}
        response = make_response(records_to_text(params, data, ","), headers)
        response.headers["Content-type"] = "text/csv"
    elif params["format"] == "json":
        response = make_response(records_to_json(params, data))
        response.headers["Content-type"] = "application/json"
    logging.debug(f"Response built in {tictac(tic)} seconds.")
    return response


def map_requests(params, data):

    with open("./static/countries.geo.json") as json_file:
        geojson = json.load(json_file)

    geodata = dict(x=[], y=[], name=[], requests=[])
    for country in geojson["features"]:
        if country["geometry"]["type"] not in ["Polygon", "MultiPolygon"]:
            break

        if country["geometry"]["type"] == "Polygon":
            country["geometry"]["coordinates"] = [country["geometry"]["coordinates"]]

        for polygon in country["geometry"]["coordinates"]:
            geodata["x"].append([x[0] for x in polygon[0]])
            geodata["y"].append([x[1] for x in polygon[0]])
            geodata["name"].append(country["properties"]["name"].lower())
    geodata["requests"] = [0 for i in range(0, len(geodata["name"]))]

    for code, requests in data:
        country = codes.get(code)
        if country and requests:
            for i, name in enumerate(geodata["name"]):
                if country.lower() in name:
                    geodata["requests"][i] += requests

    my_palette = ("#f2f2f2",) + Viridis256
    # my_palette = ("#f2f2f2",) + tuple(reversed(Plasma256))
    # my_palette = ("#fee5d9", "#fcbba1", "#fc9272", "#fb6a4a", "#de2d26")
    color_mapper = LogColorMapper(palette=("#f2f2f2",) + my_palette, low=0.1)
    if sum(geodata["requests"]) == 0:
        color_mapper = None

    TOOLS = "save, pan, wheel_zoom, box_zoom, reset"
    plot = figure(
        title="Number of requests to the RESIF data center",
        tools=TOOLS,
        plot_width=1200,
        plot_height=800,
        x_axis_location=None,
        y_axis_location=None,
        tooltips=[
            ("Name", "@name"),
            ("Requests", "@requests"),
            ("(Long, Lat)", "($x, $y)"),
        ],
        active_drag="box_zoom",
        active_scroll="wheel_zoom",
        # sizing_mode='stretch_both'
    )

    plot.toolbar.logo = None
    plot.grid.grid_line_color = None
    plot.hover.point_policy = "follow_mouse"
    plot.patches(
        "x",
        "y",
        source=geodata,
        fill_color={"field": "requests", "transform": color_mapper},
        line_width=0.2,
        fill_alpha=0.8,
        # line_color="white",
    )

    color_bar = ColorBar(
        color_mapper=color_mapper,
        ticker=LogTicker(),
        formatter=NumeralTickFormatter(),
        label_standoff=5,
        width=500,
        height=10,
        location=(0, 0),
        orientation="horizontal",
    )

    plot.add_layout(color_bar, "below")
    return make_response(file_html(plot, CDN, "resifws-statistics"))


def get_output(params):
    """Statistics output (csv, request, sync, text)
    :params: parameters
    :returns: text or csv with data statistics"""

    try:
        tic = time.time()
        data = collect_data(params)
        if data is None:
            return data

        if not data:
            code = params["nodata"]
            return error_request(msg=f"HTTP._{code}_", details=Error.NODATA, code=code)
        logging.info(f"Number of collected rows: {len(data)}")

        if params["request"] == "map":
            return map_requests(params, data)
        data = format_results(params, data)
        return get_response(params, data)
    except Exception as ex:
        logging.exception(str(ex))
    finally:
        logging.info(f"Data processed in {tictac(tic)} seconds.")
