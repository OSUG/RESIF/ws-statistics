class Parameters:
    def __init__(self):
        self.network = None
        self.station = None
        self.location = None
        self.channel = None
        self.starttime = None
        self.endtime = None
        self.net = "*"
        self.sta = "*"
        self.loc = "*"
        self.cha = "*"
        self.start = None
        self.end = None
        self.country = "*"
        self.year = None
        self.type = None
        self.request = None
        self.media = None
        self.orderby = "nslc"
        self.ranklimit = None
        self.sum = "False"
        self.format = "text"
        self.nodata = "204"
        self.constraints = {
            "alias": [
                ("network", "net"),
                ("station", "sta"),
                ("location", "loc"),
                ("channel", "cha"),
                ("starttime", "start"),
                ("endtime", "end"),
            ],
            "booleans": ["sum"],
            "floats": ["ranklimit"],
            "not_none": ["request"],
        }

    def todict(self):
        return self.__dict__
