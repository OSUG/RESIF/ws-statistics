import logging
import queue
from multiprocessing import Process, Queue

from flask import request

from apps.globals import Error
from apps.globals import HTTP
from apps.globals import TIMEOUT
from apps.output import get_output
from apps.parameters import Parameters
from apps.utils import check_base_parameters, is_valid_send_request
from apps.utils import check_request
from apps.utils import error_param
from apps.utils import error_request
from apps.utils import is_valid_country
from apps.utils import is_valid_datatype
from apps.utils import is_valid_media
from apps.utils import is_valid_nodata
from apps.utils import is_valid_orderby
from apps.utils import is_valid_output
from apps.utils import is_valid_request
from apps.utils import is_valid_year


def check_parameters(params):

    # check base parameters
    (params, error) = check_base_parameters(params)
    if error["code"] != 200:
        return (params, error)

    country = params["country"].split(",")
    for c in country:
        if not is_valid_country(c):
            return error_param(params, Error.COUNTRY + c)

    # orderby parameter validation
    if params["orderby"] is not None:
        if not is_valid_orderby(params["orderby"]):
            return error_param(params, Error.ORDERBY + str(params["orderby"]))
        params["orderby"] = params["orderby"].lower()
    else:
        params["orderby"] = "nslc"

    # year parameter validation
    if params["year"]:
        years = params["year"].split(",")
        for year in years:
            if not is_valid_year(year):
                return error_param(params, Error.YEAR + str(year))

    # media parameter validation
    if params["media"]:
        medias = params["media"].split(",")
        for media in medias:
            if not is_valid_media(media):
                return error_param(params, Error.MEDIA + str(media))
        params["media"] = params["media"].lower()

    # data type parameter validation
    if params["type"]:
        types = params["type"].split(",")
        for datatype in types:
            if not is_valid_datatype(datatype):
                return error_param(params, Error.DATATYPE + str(datatype))
        params["type"] = params["type"].lower()

    # request parameter validation
    if params["request"]:
        if not is_valid_request(params["request"]):
            return error_param(params, Error.REQUEST + str(params["request"]))
        # Selon le paramètre media, toutes les requêtes ne sont pas valides
        if not is_valid_send_request(params["media"]):
            return error_param(params, Error.SEND_REQUEST)

        params["request"] = params["request"].lower()

    # country="all" has no meaning in list
    if "all" in params["country"] and params["country"] != "all":
        return error_param(params, Error.COUNTRY_ALL + str(params["country"]))

    # output parameter validation
    if not is_valid_output(params["format"]):
        return error_param(params, Error.OUTPUT + str(params["format"]))
    params["format"] = params["format"].lower()

    # nodata parameter validation
    if not is_valid_nodata(params["nodata"]):
        return error_param(params, Error.NODATA_CODE + str(params["nodata"]))
    params["nodata"] = params["nodata"].lower()

    for key, val in params.items():
        logging.debug(key + ": " + str(val))

    return (params, {"msg": HTTP._200_, "details": Error.VALID_PARAM, "code": 200})


def checks_get():

    # get default parameters
    params = Parameters().todict()

    # check if the parameters are unknown
    (p, result) = check_request(params)
    if result["code"] != 200:
        return (p, result)
    return check_parameters(params)


def output():

    try:
        process = None
        result = {"msg": HTTP._400_, "details": Error.UNKNOWN_PARAM, "code": 400}
        logging.debug(request.url)

        (params, result) = checks_get()
        if result["code"] == 200:

            def put_response(q):
                q.put(get_output(params))

            q = Queue()
            process = Process(target=put_response, args=(q,))
            process.start()
            resp = q.get(timeout=TIMEOUT)

            if resp:
                return resp
            else:
                raise Exception

    except queue.Empty:
        result = {"msg": HTTP._408_, "details": Error.TIMEOUT, "code": 408}

    except Exception as excep:
        result = {"msg": HTTP._500_, "details": Error.UNSPECIFIED, "code": 500}
        logging.exception(str(excep))

    finally:
        if process:
            process.terminate()

    return error_request(
        msg=result["msg"], details=result["details"], code=result["code"]
    )
