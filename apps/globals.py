STRING_TRUE = ("yes", "true", "t", "y", "1", "")
STRING_FALSE = ("no", "false", "f", "n", "0")
REQUEST = ("storage", "send", "country", "timeseries", "map")
DATATYPE = ("buffer", "validated")
ORDERBY = ("nslc", "time", "country", "protocol")
OUTPUT = ("csv", "json", "text")
MEDIA = ("dataselect", "seedlink", "station")
NODATA_CODE = ("204", "404")
TIMEOUT = 120

# error message constants
DOCUMENTATION_URI = "http://ws.resif.fr/resifws/statistics/1/"
SERVICE = "resifws-statistics"
VERSION = "1.0.0"


class Error:
    UNKNOWN_PARAM = "Unknown query parameter: "
    MULTI_PARAM = "Multiple entries for query parameter: "
    VALID_PARAM = "Valid parameters."
    START_LATER = "The starttime cannot be later than the endtime: "
    TOO_LONG_DURATION = "Too many days requested (greater than "
    UNSPECIFIED = "Error processing your request."
    NODATA = "Your query doesn't match any available data."
    TIMEOUT = f"Your query exceeds timeout ({TIMEOUT} seconds)."
    MISSING = "Missing parameter: "
    BAD_VAL = " Invalid value: "
    CHAR = "White space(s) or invalid string. Invalid value for: "
    EMPTY = "Empty string. Invalid value for: "
    BOOL = "(Valid boolean values are: true/false, yes/no, t/f or 1/0)"
    NETWORK = "Invalid network code: "
    STATION = "Invalid station code: "
    LOCATION = "Invalid location code: "
    CHANNEL = "Invalid channel code: "
    COUNTRY = "Invalid country code: "
    YEAR = "Invalid year: "
    TIME = "Bad date value: "
    DATATYPE = f"Accepted type values are: {DATATYPE}." + BAD_VAL
    REQUEST = f"Accepted request values are: {REQUEST}." + BAD_VAL
    SEND_REQUEST = f"Request 'send' can not be used with parameter media=station"
    MEDIA = f"Accepted media values are: {MEDIA}." + BAD_VAL
    ORDERBY = f"Accepted orderby values are: {ORDERBY}." + BAD_VAL
    OUTPUT = f"Accepted output values are: {OUTPUT}." + BAD_VAL
    NODATA_CODE = f"Accepted nodata values are: {NODATA_CODE}." + BAD_VAL
    NO_SELECTION = "Request contains no selections."
    NO_WILDCARDS = "Wildcards or lists are not allowed." + BAD_VAL
    COUNTRY_ALL = "The special option country=all has no meaning in list." + BAD_VAL


class HTTP:
    _200_ = "Successful request. "
    _204_ = "No data matches the selection. "
    _400_ = "Bad request due to improper value. "
    _401_ = "Authentication is required. "
    _403_ = "Forbidden access. "
    _404_ = "No data matches the selection. "
    _408_ = "Request exceeds timeout. "
    _413_ = "Request too large. "
    _414_ = "Request URI too large. "
    _500_ = "Internal server error. "
    _503_ = "Service unavailable. "
