# Webservice statistics

## Go on with Flask

    FLASK_ENV=development RUNMODE=staging PGUSER=resifstats_ro PGHOST=resif-pgprod.u-ga.fr PGPORT=5432 PGDATABASE=resifstats PGPASSWORD=XXXXXXXX FLASK_APP=start:app python start.py

## Play around with docker

```
docker build -t ws-statistics .
docker run --rm -e RUNMODE=test -p 8000:8000 --name ws-statistics ws-statistics:latest
```

Then :

```
wget -O - http://localhost:8000/1/application.wadl
```

Run it in debug mode with flask :

```
docker build -t ws-statistics .
docker run --rm --name ws-statistics -e RUNMODE=production ws-statistics
```

## RUNMODE builtin values

  * `production`
  * `test`
  * `local`
  * other values map to :
